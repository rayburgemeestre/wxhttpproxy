#pragma once

#include "outputhandler.h"
#include "indicators.h"
#include "wx/string.h"

class ConsoleOutputHandler : public OutputHandler
{
public:

    bool UseCache();

    void CacheUri(wxString &wxstr);

    std::string FindUri(long item);

    void ClearUris();


    // Empty impl
    bool IsIntercepting() { return true; };
    size_t AddIndicator() { return 0; }
    void RemoveIndicator(size_t indicator) {}
    void SetIndicatorLabel(size_t indicator, wxString label) {}
    void SetSecure(size_t indicator) {}

    void HandleLog(const wxString &msg) {}
    void HandleClearAll() {}

private:

};