#pragma once

#include <string>
#include "indicators.h"

class wxString;

class OutputHandler : public Indicators
{
public:

    virtual bool UseCache() = 0;

    virtual void CacheUri(wxString &wxstr) = 0;

    virtual std::string FindUri(long item) = 0;

    virtual void ClearUris() = 0;

    virtual bool IsIntercepting() = 0;

    virtual void HandleLog(const wxString &msg) = 0;

    virtual void HandleClearAll() = 0;

};
