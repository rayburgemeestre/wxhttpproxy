#pragma once

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include <memory>
#include "wx/socket.h"

#include "httpbuffer.h"
#include "logger.h"

class wxIPV4address;
class wxSocketClient;
class SocketServer;
class Indicators;
class Cache;
class CachedRequest;
class OutputHandler;

class ClientData 
{
public:

	enum {
		WAITING_FOR_CONNECT = 1, // waiting for client to send connect request
		WAITING_FOR_CONNECTION, // waiting for the requested host is connected
		HOST_CLIENT_CONNECTED,
		WAITING_FOR_ACTUAL_REQUESTDATA,
		CLIENT_DISCONNECTED
	} STATES;

	ClientData(
		SocketServer * const socketServer,
		wxSocketBase *clientSocket,
		int uniqueId,
		Indicators &indicators,
		Cache &cacheProvider,
		OutputHandler &outputHandler,
		Logger &logger
	);
	virtual ~ClientData();

	void StoreBuffer(const char *, size_t len);
	void FlushBuffer();

	void SetClientDisconnected();

	int GetState();
	void SetState(int state);

	wxSocketBase *GetClientSocket();
	httpbuffer &GetHttpBuffer();

	void SetFirstByteReceived(bool val = true);
	bool IsFirstByteReceived();
	bool IsSSLConnection();
	void ConnectToHost();
	void ReconnectToHost();

	int GetId();

	std::shared_ptr<CachedRequest> GetCache();

private:

	Logger &logger_;
	int clientState_;

	// The client (the proxy user -> us)
	wxSocketBase *clientSocket_;
	httpbuffer clientSocketBuffer_;
	std::string clientToHostBuffer_;

	// Host client (us -> requested host)
	wxSocketClient *hostClient_;
	httpbuffer hostClientBuffer_;

	SocketServer * const socketServer_;
	OutputHandler &outputHandler_;

	int uniqueId_;
	bool isSecureConnection_;
	bool isSecureCommunicationActive_;
	bool firstByteReceived_;

	std::string hostDomain_;
	int hostPort_;

	int numRequests_;
	Indicators &indicators_;

	Cache &cache_;

	std::shared_ptr<CachedRequest> currentCachedRequest_;
};