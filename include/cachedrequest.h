#pragma once

#include <iostream>
#include <string>
#include <sstream>

class CachedRequest {
public:

	CachedRequest(const std::string &uri)
		: uri_(uri), enabled_(false), finalized_(false), convertback_(false)
	{}

	void append(const std::string &data) {
		data_.append(data);
	}

	std::string uri() {
		return uri_;
	}

	std::string data() {
		return data_;
	}

	void assign(const std::string &newdata) {
		data_.assign(newdata);
	}

	void enable(bool value) {
		enabled_ = value;
	}

	bool enabled() {
		return enabled_;
	}

	void finalize(bool value = true) {
		finalized_ = value;
	}

	bool finalized() {
		return finalized_;
	}

	void normalizeHeaderLineEndings() {
		std::stringstream ss;
		std::istringstream iss2(data_);
		bool headersPassed = false;
		for (std::string line; std::getline(iss2, line);) {
			if (!headersPassed) {
				// normalize line endings
				if (line.length() > 0 && line[line.length() - 1] == '\n')
					line.erase(line.size() - 1);
				if (line.length() > 0 && line[line.length() - 1] == '\r')
					line.erase(line.size() - 1);
				ss << line << std::endl;
			}
			else {
				ss << line << std::endl;
			}
			if (line == "" || line == "\r") {
				headersPassed = true;
			}
		}
		data_.assign(ss.str());
	}

	void updateContentLengthHeader() {
		std::string result;
		std::istringstream iss(data_);

		size_t size = 0;
		bool count = false;
		for (std::string line; std::getline(iss, line);) {
			if (count) {
				size += line.length() + 1 /* the newline */;
			}
			if (line == "" || line == "\r") {
				count = true;
			}

		}

		std::stringstream ss;
		std::istringstream iss2(data_);
		for (std::string line; std::getline(iss2, line);) {
			if (line.find("Content-Length:") != std::string::npos) {
				count += line.length() + 1 /* the newline */;
				ss << "Content-Length: " << size;
				if (line.at(line.length() - 1) == '\r') {
					ss << "\r";
				}
				ss << std::endl;
			} else {
				ss << line << std::endl;
			}
		}
		data_.assign(ss.str());
	}

	void setConvertBack(bool flag) {
		convertback_ = flag;
	}

	bool convertBack() {
		return convertback_;
	}

private:

	bool enabled_;
	bool finalized_;
	std::string uri_;
	std::string data_;
	bool convertback_;
};
