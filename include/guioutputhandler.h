#pragma once

#include <map>
#include <string>

#include "outputhandler.h"

class wxListCtrl;
class wxCheckBox;

class GuiOutputHandler : public OutputHandler
{
public:

    GuiOutputHandler(
		wxPanel &panelTemplateIndicators,
		wxBoxSizer &sizerTemplateIndicators_,
        wxListCtrl &textDetailLog,
        wxListCtrl &listctrlMetaLog,
        wxListCtrl &listctrlDetailLog,
        wxListCtrl &listctrlCachedRequests,
        wxCheckBox &checkboxIntercept,
        wxCheckBox &checkboxUseCache
    );


    bool UseCache();

    void CacheUri(wxString &wxstr);

    std::string FindUri(long item);

    void ClearUris();

    bool IsIntercepting();

    void HandleLog(const wxString &msg);

    void HandleClearAll();


    // Indicators
    size_t AddIndicator();
    void RemoveIndicator(size_t indicator);
    void SetIndicatorLabel(size_t indicator, wxString label);
    void SetSecure(size_t indicator);

private:

    void UpdateMetaMap();

    // was part of Logger
    void InsertItem(wxListCtrl *listctrl, const wxString &msg);
    std::map<size_t, std::string> longMessages;
    //--


    std::map<long, std::string> listctrlIndexToRequestUriMap_;

    wxListCtrl &textDetailLog_;
    wxListCtrl &listctrlMetaLog_;
    wxListCtrl &listctrlDetailLog_;
    wxListCtrl &listctrlCachedRequests_;
    wxCheckBox &checkboxIntercept_;
    wxCheckBox &checkboxUseCache_;
    wxPanel &panelTemplateIndicators_;
    wxBoxSizer &sizerTemplateIndicators_;
};
