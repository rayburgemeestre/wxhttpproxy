#pragma once

#include <map>
#include <memory>
#include "cachedrequest.h"


class OutputHandler;

class Cache
{
public:
	
	Cache(OutputHandler &outputHandler);
	
	std::shared_ptr<CachedRequest> GetCachedRequestByUri(const std::string &uri);
	std::shared_ptr<CachedRequest> CreateNewRequestForUri(const std::string &uri);
	
	void RegisterCachedRequestInGuiListCtrl(std::shared_ptr<CachedRequest> cache);
	
	std::shared_ptr<CachedRequest> GetCachedRequestByListCtrlIndex(long item);
	
	void ClearAll();


private:
	
	std::map<std::string, std::shared_ptr<CachedRequest>> cachedRequestsMap_;
	
	OutputHandler &outputHandler_;
};
