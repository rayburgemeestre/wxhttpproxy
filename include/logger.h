#pragma once

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include <string>
#include <map>
#include <fstream>

class wxListCtrl;
class OutputHandler;

extern std::map<size_t, size_t> mapMetaLogItemsToDetailLogItems;
extern bool autoFollowLastLineInListctrls;
class Logger
{
public:
	
	Logger(
        OutputHandler &outputHandler
	);
	
	~Logger();
	
	void Log(const std::string &msg);
	void Log(const wxString &msg);
	
	const std::map<size_t, size_t> &GetMapping() const { return mapMetaLogItemsToDetailLogItems; }
	void SetAutoFollow(bool val) { autoFollowLastLineInListctrls = val; }

	void SetOutputFile(std::string file, long truncateLines);
	void SetNoStdout(bool flag);
	
	void ClearAll();
	
private:
	
	std::string initiator_;

	OutputHandler &outputHandler_;

	std::ofstream outputFileHandle_;
	std::string outputFile_;
	long truncateLines_;
	long outputtedLines_;
	bool quiet_;
};