#include "clientdata.h"

#include "socketserver.h"
#include "indicators.h"
#include "cache.h"
#include "cachedrequest.h"

ClientData::ClientData(
	SocketServer * const socketServer,
	wxSocketBase *clientSocket,
	int uniqueId,
	Indicators &indicators,
	Cache &cacheProvider,
	OutputHandler &outputHandler,
	Logger &logger
)
	: logger_(logger),
	  socketServer_(socketServer),
	  clientSocket_(clientSocket),
	  hostClient_(new wxSocketClient()),
	  outputHandler_(outputHandler),
	  clientState_(WAITING_FOR_CONNECT),
	  uniqueId_(uniqueId),
	  isSecureConnection_(false),
	  isSecureCommunicationActive_(false),
	  hostPort_(80),
	  indicators_(indicators),
	  numRequests_(0),
	  cache_(cacheProvider),
	  currentCachedRequest_(new CachedRequest("")) // unfortunately, cannot use nullptr
{
}

ClientData::~ClientData() 
{
	clientState_ = 0;
	clientSocket_ = NULL;
	hostClient_ = NULL;
}

void ClientData::SetClientDisconnected()
{
	clientState_ = CLIENT_DISCONNECTED;
}

httpbuffer &ClientData::GetHttpBuffer()
{
	return clientSocketBuffer_;
}

void ClientData::StoreBuffer(const char *buffer, size_t len)
{
	if (clientState_ == CLIENT_DISCONNECTED)
		return;

	GetHttpBuffer().append(buffer, len);
	FlushBuffer();
}

void ClientData::FlushBuffer()
{
	if (clientState_ == CLIENT_DISCONNECTED)
		return;
	
	// Don't bother parsing SSL :-)
	if (isSecureConnection_ && isSecureCommunicationActive_) {
		std::string raw = clientSocketBuffer_.get_raw();
		logger_.Log(wxString::Format(_("Thread #%d - Reading raw data from secure connection, of length: %ld\n"), uniqueId_, raw.length()));
		clientToHostBuffer_.append(raw);
	}
	else {
		
		int uniqueId = *(int *)GetClientSocket()->GetClientData();
	
		while (clientSocketBuffer_.parse_line())
			;

		int i = 0;
		while (true) {
			std::string command = clientSocketBuffer_.pop();
			if (command == "")
				break;

			if (command == "CONNECT") {
				std::string oldDomain = hostDomain_;
				int oldPort = hostPort_;

				hostDomain_ = clientSocketBuffer_.pop();
				std::string port = clientSocketBuffer_.pop();
				hostPort_ = atoi(port.c_str());

				// If this is a reconnect, trick the socket into thinking it's a "new" connection
				//  (will result in some extra output , like "Response from host." message)
				SetFirstByteReceived(false);

				if (hostPort_ == 443) {
					isSecureConnection_ = true;
					isSecureCommunicationActive_ = false;
				}

				//@Todo re-use sockets..
				logger_.Log(wxString::Format(_("Thread #%d - Connecting to host %s:%s%d for client.\n"),
						uniqueId,
						wxString(hostDomain_.c_str(), wxConvUTF8).c_str(),
						isSecureConnection_ ? wxT("+") : wxT(""),
						hostPort_));

				// Is there also a place necessary for SSL ?? See comment below?? (in == 443, statement..
				if (clientState_ != WAITING_FOR_CONNECT && oldDomain != hostDomain_ || oldPort != hostPort_) {
					logger_.Log(wxString::Format(_("Thread #%d - Warning request for different backend for thread (%s:%d --> %s:%d), reconnecting host socket..\n"),
						uniqueId,
						wxString(oldDomain.c_str(), wxConvUTF8).c_str(),
						oldPort,
						wxString(hostDomain_.c_str(), wxConvUTF8).c_str(),
						hostPort_

					));
					ReconnectToHost();
				}

				if (hostPort_ == 443) {
					// When using a CONNECT all the following headers are for us (the proxy), so do not connect before we've received all headers.
					continue;
				}
				
			}
			else if (command == "LINE") {
				std::string line = clientSocketBuffer_.pop();

				wxString wstr(line.c_str(), wxConvUTF8);
				if (wstr.StartsWith(_("GET")) || wstr.StartsWith(_("POST")) || wstr.StartsWith(_("PUT")) || wstr.StartsWith(_("DELETE")) || wstr.StartsWith(_("HEAD")) || wstr.StartsWith(_("OPTIONS"))) {
					logger_.Log(wxString::Format(_("Thread #%d - Request from client: %s"), uniqueId, wstr.c_str()));
					indicators_.SetIndicatorLabel(uniqueId_, wxString::Format(wxT("%d"), numRequests_));
					numRequests_++;

					// Convert GET http://cppse.nl/test.html HTTP/1.0 into
					// GET /test.html HTTP/1.0
					// So we don't recognize the two syntaxes as different cache entries.
					size_t pos = wstr.find(_(" "));
					if (pos != std::string::npos) {
						wxString method(wstr.substr(0, pos));
						wxString rest(wstr.SubString(pos + 1, wstr.length()));
						// GET http://cppse.nl/abba.html HTTP1/1.0
						if (rest.StartsWith(_("http://")) || rest.StartsWith(_("HTTP://"))) {
							rest = rest.SubString(std::string("http://").length(), rest.length());
							pos = rest.find(_("/"));
							if (pos != std::string::npos) {
								rest = rest.SubString(pos, rest.length());
								wstr.assign(wxString::Format(wxT("%s %s"), method.c_str(), rest.c_str()));

								// Temp workaround(!)
								line = wstr;
								logger_.Log(wxString::Format(_("Thread #%d - Request from client: %s (workaround fix!)"), uniqueId, wstr.c_str()));
							}
						}
					}

					if (currentCachedRequest_.get()->uri() != "") {
						currentCachedRequest_->finalize();
					}

					std::string strRequest(
						wxString(
							wxString::Format(_("%s:%s%d - %s"),
								wxString(hostDomain_.c_str(), wxConvUTF8).c_str(),
								isSecureConnection_ ? wxT("+") : wxT(""),
								hostPort_,
								wstr.c_str()
							)
						).char_str()
					);

					currentCachedRequest_ = cache_.GetCachedRequestByUri(strRequest);

					// Overwrite the cache if we haven't locked it :-)
					if (currentCachedRequest_.get()->uri() != "" && !currentCachedRequest_->enabled()) {
						currentCachedRequest_->assign("");
						currentCachedRequest_->finalize(false);
					}

					// Send cached version to client
					if (!isSecureConnection_ && currentCachedRequest_.get()->uri() != "" && currentCachedRequest_->enabled()) {

						GetCache()->updateContentLengthHeader();

						char *buffer = const_cast<char *>(GetCache()->data().c_str());
						char *pcurrent = buffer;
						size_t bytesRead = GetCache()->data().length();
						size_t processed = 0;
						do {
							GetClientSocket()->Write(pcurrent, bytesRead - processed);
							if (GetClientSocket()->Error()) {
								std::cout << "error writing: " << GetClientSocket()->LastError() << std::endl;
								break;
							}
							processed += GetClientSocket()->LastCount();
							pcurrent = buffer + processed;
						}
						while (processed < bytesRead);

						// Empty what's left in socket buffer
						while (clientSocketBuffer_.pop() != ""); 

						return;
					}
					else {

						if (currentCachedRequest_.get()->uri() == "") {
							currentCachedRequest_ = cache_.CreateNewRequestForUri(strRequest);
							cache_.RegisterCachedRequestInGuiListCtrl(GetCache());
						}

						ConnectToHost();
					}
				}

				clientToHostBuffer_.append(line);

				if (wstr.Trim(true) == wxT("") && isSecureConnection_) {
					// The buffered data was for us (the proxy), we don't care about these headers, so clear them.
					clientToHostBuffer_.assign("");
					ConnectToHost();

					// Make sure we don't try to parse line for line, as it's encrypted binary data
					isSecureCommunicationActive_ = true;

					indicators_.SetSecure(mapMetaLogItemsToDetailLogItems[uniqueId_]);
				}
			}
		}
	}

	if (clientState_ == HOST_CLIENT_CONNECTED) {

		socketbuffer buf;
		buf.append(clientToHostBuffer_.c_str(), clientToHostBuffer_.length());
		while (buf.has_line()) {
			logger_.Log(wxString::Format(_("Thread #%d - >>>: %s"), uniqueId_, wxString(buf.get_line().c_str(), wxConvUTF8).c_str()));
		}
		if (buf.length()) {
			logger_.Log(wxString::Format(_("Thread #%d - >>>: %s (incomplete)\n"), uniqueId_, wxString(buf.get_raw().c_str(), wxConvUTF8).c_str()));
		}

		// Temporary hack that needs to replaced with an actual feature, to make sure we don't send gzip and deflate as acceptable encodings
		// as I've not yet implemented these ;-)
		if (!isSecureConnection_) {
			wxString temp(clientToHostBuffer_.c_str(), wxConvUTF8);
			temp.Replace(_("gzip"), _("nozp"), true);
			temp.Replace(_("deflate"), _("noflate"), true);
			clientToHostBuffer_.assign((const char *)temp.char_str());
		}
		// Once connected, write to host...
		char *pcurrent = const_cast<char *>(clientToHostBuffer_.c_str());
		size_t processed = 0;
		do {
			hostClient_->Write(pcurrent, clientToHostBuffer_.length() - processed);
			processed += hostClient_->LastCount();

			pcurrent = const_cast<char *>(clientToHostBuffer_.c_str()) + processed;
		}
		while (processed < clientToHostBuffer_.length());

		clientToHostBuffer_ = "";
	}
}



void ClientData::ConnectToHost()
{
	wxIPV4address addr;
	addr.Hostname(wxString(hostDomain_.c_str(), wxConvUTF8));
	addr.Service(hostPort_);

	// Setup the event handler and subscribe to most events
	hostClient_->SetEventHandler(*socketServer_, SocketServer::HOST_ID);
	hostClient_->SetNotify(wxSOCKET_CONNECTION_FLAG |
	                       wxSOCKET_INPUT_FLAG |
	                       wxSOCKET_OUTPUT_FLAG |
	                       wxSOCKET_LOST_FLAG);
	hostClient_->Notify(true);
	hostClient_->SetClientData((void *)new int(uniqueId_));
	hostClient_->Connect(addr, false);

	clientState_ = WAITING_FOR_CONNECTION;
}

void ClientData::ReconnectToHost()
{
	wxIPV4address addr;
	addr.Hostname(wxString(hostDomain_.c_str(), wxConvUTF8));
	addr.Service(hostPort_);

	hostClient_->Close();
	hostClient_->Connect(addr, false);

	clientState_ = WAITING_FOR_CONNECTION;
}

int ClientData::GetId()
{
	return uniqueId_;
}

void ClientData::SetFirstByteReceived(bool val)
{
	firstByteReceived_ = val;
}
bool ClientData::IsFirstByteReceived() 
{
	return firstByteReceived_;
}

int ClientData::GetState()
{
	return clientState_;
}

void ClientData::SetState(int st)
{
	clientState_ = st;
}

wxSocketBase * ClientData::GetClientSocket()
{
	return clientSocket_;
}

bool ClientData::IsSSLConnection()
{
	return isSecureConnection_;
}

std::shared_ptr<CachedRequest> ClientData::GetCache()
{
	return currentCachedRequest_;
}
