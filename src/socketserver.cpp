#include "socketserver.h"

#include "clientdata.h"

#include "indicators.h"

BEGIN_EVENT_TABLE(SocketServer, wxEvtHandler)
	EVT_SOCKET(SocketServer::SERVER_ID, SocketServer::OnServerEvent)
	EVT_SOCKET(SocketServer::SOCKET_ID, SocketServer::OnSocketEvent)
	EVT_SOCKET(SocketServer::HOST_ID, SocketServer::OnHostClientEvent)
END_EVENT_TABLE()

wxIPV4address port_to_wxIPV4address(unsigned short port)
{
	wxIPV4address addr;
	addr.Service(port);
	return std::move(addr);
}

#include "outputhandler.h"

SocketServer::SocketServer(
		int port,
		OutputHandler &outputHandler,
		Logger &logger
	)
: socketServer_(port_to_wxIPV4address(port), wxSOCKET_REUSEADDR),
  logger_(logger),
  indicators_(outputHandler),
  outputHandler_(outputHandler),
  Cache(outputHandler)
{
	if (!socketServer_.Ok()) {
		logger_.Log(wxString::Format(_("Error - Could not listen at the specified port %d!\n"), port));
		return;
	} else {
		logger_.Log(wxString::Format(_("Info - Server listening on port %d.\n"), port));
	}

	// Setup the event handler and subscribe to connection events
	socketServer_.SetEventHandler(*this, SocketServer::SERVER_ID);
	socketServer_.SetNotify(wxSOCKET_CONNECTION_FLAG);
	socketServer_.Notify(true);
}

SocketServer::~SocketServer()
{
}

void SocketServer::OnServerEvent(wxSocketEvent& event)
{
	if (event.GetSocketEvent() != wxSOCKET_CONNECTION) {
		logger_.Log(wxString::Format(_("Error - unexpected socket event received: %d\n"), event.GetSocketEvent()));
	}

	wxSocketBase *sock = socketServer_.Accept(false);

	static int uniqueId = 0;

	if (sock) {
		uniqueId++;
		logger_.Log(wxString::Format(_("Thread #%d - New client connection accepted\n"), uniqueId));
	} else {
		logger_.Log(wxString::Format(_("Error - Couldn't accept a new connection\n"), uniqueId));
		return;
	}

	clientData_[uniqueId] = new ClientData(
		this, sock, uniqueId,
		indicators_,
		*this,
		outputHandler_,
		logger_
	);
	mapClientsToIndicators_[uniqueId] = indicators_.AddIndicator();

	sock->SetClientData((void *) new int(uniqueId));
	sock->SetEventHandler(*this, SocketServer::SOCKET_ID);
	sock->SetNotify(wxSOCKET_INPUT_FLAG | wxSOCKET_OUTPUT_FLAG | wxSOCKET_LOST_FLAG);
	sock->Notify(true);
}

void SocketServer::OnSocketEvent(wxSocketEvent& event)
{
	wxSocketBase *sock = event.GetSocket();

#ifdef LOG_EVENTS
	wxString s = _("OnSocketEvent - ");
	s.Append(wxString::Format(_(" (%d) "), event.GetId()));

	// First, print a message
	switch (event.GetSocketEvent()) {
		case wxSOCKET_INPUT:
			s.Append(_("wxSOCKET_INPUT"));
			break;
		case wxSOCKET_OUTPUT:
			s.Append(_("wxSOCKET_OUTPUT"));
			break;
		case wxSOCKET_LOST:
			s.Append(_("wxSOCKET_LOST"));
			break;
		default:
			s.Append(_("Unexpected event"));
			break;
	}
#endif

	int cdidx = *(int *) sock->GetClientData();
	ClientData *clientData = clientData_[cdidx];

#ifdef LOG_EVENTS
	logger_.Log(wxString::Format(_("Thread #%d - "), clientData->GetId()) + s + _("\n"));
#endif

	// Now we process the event
	switch (event.GetSocketEvent()) {
		case wxSOCKET_INPUT:
		{
			char buffer[10240 + 1] = {0x00};
			sock->Read(buffer, 10240);
			//@Todo: In wxWidgets 2.9 change to LastReadCount()!!
			wxUint32 bytesRead = sock->LastCount();

			if (sock->Error()) {
				logger_.Log(
					_("Error - There was an error reading from client, discarding the read op.\n")
				);
				break;
			}

			clientData->StoreBuffer(buffer, bytesRead);

			sock->SetNotify(wxSOCKET_LOST_FLAG | wxSOCKET_INPUT_FLAG);

			break;
		}
		case wxSOCKET_OUTPUT:
		{
			// logger_.log(_("INFO - Outputting to client connection.....\n"));
			break;
		}
		case wxSOCKET_LOST:
		{

			int cdidx = *(int *) sock->GetClientData();
			ClientData * clientData(clientData_[cdidx]);
			clientData->FlushBuffer();
			clientData->SetClientDisconnected();

			logger_.Log(wxString::Format(_("Thread #%d - Client connection lost.\n"), clientData->GetId()));

			indicators_.RemoveIndicator(mapClientsToIndicators_[cdidx]);
			mapClientsToIndicators_.erase(cdidx);

			/**
			 * Frames and dialogs are not destroyed immediately when this 
			 * function is called -- they are added to a list of windows to be
			 * deleted on idle time, when all the window's events have been
			 * processed. This prevents problems with events being sent to 
			 * non-existent windows.
			 * 
			 * @param event
			 */

			delete(clientData);
			clientData_[cdidx] = NULL;
			clientData_.erase(cdidx);
			sock->Destroy();
			break;
		}
		default:
			break;
	}
}

void SocketServer::OnHostClientEvent(wxSocketEvent& event)
{
	wxSocketBase *sock = event.GetSocket();
	int cdidx = *(int *) sock->GetClientData();

#ifdef LOG_EVENTS
	wxString s = _("OnHostClientEvent - ");
	s.Append(wxString::Format(_(" (%d) "), event.GetId()));


	// First, print a message
	switch (event.GetSocketEvent()) {
		case wxSOCKET_CONNECTION:
			s.Append(_("wxSOCKET_CONNECTION"));
			break;
		case wxSOCKET_INPUT:
			s.Append(_("wxSOCKET_INPUT"));
			break;
		case wxSOCKET_OUTPUT:
			s.Append(_("wxSOCKET_OUTPUT"));
			break;
		case wxSOCKET_LOST:
			s.Append(_("wxSOCKET_LOST"));
			break;
		default:
			s.Append(_("Unexpected event !"));
			break;
	}
#endif


	if (clientData_.find(cdidx) == clientData_.end()) {
		// Not really that interesting, wxSOCKET_LOST of the Host socket means there isn't any client connection
		// either. So don't bother to log it (We also no longer have a unique Thread id to display).
#ifdef LOG_EVENTS
		//logger_.Log(s);
#endif
		return;
	}

#ifdef LOG_EVENTS
	logger_.Log(wxString::Format(_("Thread #%d - "), clientData_[cdidx]->GetId()) + " " + s);
#endif

	ClientData * clientData(clientData_[cdidx]);

	// Now we process the event
	switch (event.GetSocketEvent()) {
		case wxSOCKET_CONNECTION:
		{
			logger_.Log(wxString::Format(_("Thread #%d - Host socket connected.\n"), clientData->GetId()));

			if (clientData->IsSSLConnection()) {

				clientData->FlushBuffer();

				// Write back immediately to the client, @Todo: make utility function for this...
				char *buffer = const_cast<char *>(std::string("HTTP/1.0 200 Connection established\r\n\r\n").c_str());
				size_t buflen = strlen(buffer);
				char *pcurrent = buffer;
				size_t processed = 0;
				do {
					clientData->GetClientSocket()->Write(pcurrent, buflen - processed);
					processed += clientData->GetClientSocket()->LastCount();
					pcurrent = buffer  + processed;
				}
				while (processed < buflen);

			}

			clientData->SetState(ClientData::HOST_CLIENT_CONNECTED);
			clientData->FlushBuffer();

			break;
		}
		case wxSOCKET_INPUT:
		{
			if (!clientData->IsFirstByteReceived()) {
				logger_.Log(
					wxString::Format(
						_("Thread #%d - Response from host.\n"),
						*(int *) clientData->GetClientSocket()->GetClientData())
				);
				clientData->SetFirstByteReceived();
			}

			char buffer[10240 + 1] = {0x00};
			sock->Read(buffer, 10240);
			wxUint32 bytesRead = sock->LastCount(); //@Todo: In wxWidgets 2.9 change to LastReadCount()!!

			if (clientData->GetCache().get()->uri() != "" && !clientData->GetCache()->finalized())
				clientData->GetCache()->append(std::string(buffer, static_cast<size_t>(bytesRead)));

			if (sock->Error()) {
				logger_.Log(
					wxString::Format(_("Thread #%d - Error - Host socket Read() failed, ignoring failed call.\n"),
							clientData->GetId())
				);
				break;
			}

			socketbuffer buf;
			buf.append(buffer, bytesRead);
			int i = 0;
			while (buf.has_line()) {
				logger_.Log(
				wxString::Format(
					_("Thread #%d - <<<: %s"),
                    clientData->GetId(),
					wxString(buf.get_line().c_str(), wxConvUTF8).c_str())
				);
			}
			if (buf.length())
				logger_.Log(
				wxString::Format(
					_("Thread #%d - <<<: %s (incomplete)\n"),
                    clientData->GetId(),
					wxString(buf.get_raw().c_str(), wxConvUTF8).c_str())
				);

			// Write back immediately to the client
			char *pcurrent = buffer;
			size_t processed = 0;
			do {
				clientData->GetClientSocket()->Write(pcurrent, bytesRead - processed);
				if (clientData->GetClientSocket()->Error()) {
					logger_.Log(
						wxString::Format(_("Thread #%d - Error - Host socket Write() failed, last error = %s.\n"),
							clientData->GetId(),
							clientData->GetClientSocket()->LastError()
					));

					break;
				}
				processed += clientData->GetClientSocket()->LastCount();
				pcurrent = buffer + processed;
			}
			while (processed < bytesRead);

			sock->SetNotify(wxSOCKET_LOST_FLAG | wxSOCKET_INPUT_FLAG);

			clientData->FlushBuffer();


			break;
		}
		case wxSOCKET_OUTPUT:
		{
			break;
		}
		case wxSOCKET_LOST:
		{
			logger_.Log(wxString::Format(_("Thread #%d - Host socket disconnected.\n"), clientData->GetId()));
			clientData->SetState(ClientData::WAITING_FOR_CONNECT);
			clientData->FlushBuffer();
			sock->Destroy();
			clientData->FlushBuffer();
			// Do not Destroy()!!, Close() makes sure all bytes are written first
			clientData->GetClientSocket()->Close();

			// @Todo: fix Duplicate code here..
			indicators_.RemoveIndicator(mapClientsToIndicators_[clientData->GetId()]);
			mapClientsToIndicators_.erase(clientData->GetId());
			// end.
			break;
		}
		default:
			;
	}
}

