#include "logger.h"

#include <iostream>

// All the logger instances use a bit of global data :')
std::map<size_t, size_t> mapMetaLogItemsToDetailLogItems;
bool autoFollowLastLineInListctrls = false;

#include "outputhandler.h"

Logger::Logger(OutputHandler &outputHandler)
	: outputHandler_(outputHandler),
	  outputFile_{},
	  truncateLines_{0},
	  outputtedLines_{0},
	  quiet_{false}
{
}

Logger::~Logger()
{
	outputFileHandle_.close();
}

void Logger::SetOutputFile(std::string file, long truncateLines)
{
	outputFile_ = file;
	truncateLines_ = truncateLines;

	if (outputFileHandle_.is_open())
		outputFileHandle_.close();

	outputFileHandle_.open(file, std::ofstream::out | std::ofstream::trunc);
}

void Logger::SetNoStdout(bool flag)
{
	quiet_ = !flag;
}

void Logger::Log(const std::string &msg)
{
	wxString wmsg = wxString(msg.c_str(), wxConvUTF8);
	Log(wmsg);
}

void Logger::Log(const wxString &msg)
{
	if (!quiet_) {
		std::cout << msg.c_str();
	}

	if (!outputFile_.empty()) {
		outputFileHandle_ << msg.c_str() << std::flush;
		if (++outputtedLines_ == truncateLines_) {
			outputtedLines_ = 0;

			outputFileHandle_.close();
			outputFileHandle_.open(outputFile_, std::ofstream::out | std::ofstream::trunc);
		}
	}

	if (!outputHandler_.IsIntercepting())
		return;

	outputHandler_.HandleLog(msg);
}


void Logger::ClearAll()
{
	outputHandler_.HandleClearAll();
}
