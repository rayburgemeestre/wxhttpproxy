#if wxUSE_GUI == 1
#include "guioutputhandler.h"

#include <wx/listctrl.h>
#include <wx/checkbox.h>

// All the logger instances use a bit of global data :')

GuiOutputHandler::GuiOutputHandler(
	wxPanel &panelTemplateIndicators,
	wxBoxSizer &sizerTemplateIndicators,
    wxListCtrl &textDetailLog,
    wxListCtrl &listctrlMetaLog,
    wxListCtrl &listctrlDetailLog,
    wxListCtrl &listctrlCachedRequests,
    wxCheckBox &checkboxIntercept,
    wxCheckBox &checkboxUseCache
) : panelTemplateIndicators_(panelTemplateIndicators),
    sizerTemplateIndicators_(sizerTemplateIndicators),
    textDetailLog_(textDetailLog),
    listctrlMetaLog_(listctrlMetaLog),
    listctrlDetailLog_(listctrlDetailLog),
    listctrlCachedRequests_(listctrlCachedRequests),
    checkboxIntercept_(checkboxIntercept),
    checkboxUseCache_(checkboxUseCache)
{
}

bool GuiOutputHandler::UseCache()
{
    return checkboxUseCache_.GetValue();
}

void GuiOutputHandler::CacheUri(wxString &uri)
{
    size_t listctrlIndex = listctrlCachedRequests_.GetItemCount();
    listctrlCachedRequests_.InsertItem(listctrlIndex, uri);
    listctrlIndexToRequestUriMap_[(long)listctrlIndex] = uri;
}

std::string GuiOutputHandler::FindUri(long item)
{
    auto iter = listctrlIndexToRequestUriMap_.find(item);
    if (iter == listctrlIndexToRequestUriMap_.end()) {
        return "";
    }
    return (*iter).second;
}


void GuiOutputHandler::ClearUris()
{
    listctrlIndexToRequestUriMap_.clear();
}

bool GuiOutputHandler::IsIntercepting()
{
    return checkboxIntercept_.GetValue();
}

void GuiOutputHandler::InsertItem(wxListCtrl *listctrlPtr, const wxString &msg)
{
	size_t listctrlIndex = listctrlPtr->GetItemCount();
	bool isLongMessage = msg.length() >= 511;

	wxString msg2(wxString(msg).Trim(true));
	listctrlPtr->InsertItem(listctrlIndex, msg2);
	if (isLongMessage)
		longMessages[listctrlIndex] = std::string(msg2.char_str());

	if (isLongMessage) {
		listctrlPtr->SetItemBackgroundColour(listctrlIndex, wxColour(_("#C0C0C0")));
	} else if (msg.Contains(_("Request"))) {
        listctrlPtr->SetItemBackgroundColour(listctrlIndex, wxColour(_("#98D0E8")));
    } else if (msg.Contains(_(">>>"))) {
		listctrlPtr->SetItemBackgroundColour(listctrlIndex, wxColour(_("#BFECFF")));
	} else if (msg.Contains(_("Response"))) {
        listctrlPtr->SetItemBackgroundColour(listctrlIndex, wxColour(_("#E8DBCB")));
    } else if (msg.Contains(_("<<<"))) {
        listctrlPtr->SetItemBackgroundColour(listctrlIndex, wxColour(_("#E8E7E3")));
	} else if (msg.Contains(_("Connecting"))) {
//		listctrlPtr->SetItemBackgroundColour(listctrlIndex, wxColour(_("#A2B7F5")));
	}

	// This appears to give problems for now...
	//if (autoFollowLastLineInListctrls)
	//	listCtrlDetail_->EnsureVisible(listCtrlDetailIndex);

}

extern std::map<size_t, size_t> mapMetaLogItemsToDetailLogItems;

void GuiOutputHandler::HandleLog(const wxString &msg)
{
    if (msg.Contains(_("Request from")) || msg.Contains(_("Response from")))
        InsertItem(&listctrlMetaLog_, msg);

    InsertItem(&listctrlDetailLog_, msg);

    UpdateMetaMap();
}

void GuiOutputHandler::HandleClearAll()
{
	listctrlMetaLog_.ClearAll();
	listctrlDetailLog_.ClearAll();
	mapMetaLogItemsToDetailLogItems.clear();
}

void GuiOutputHandler::UpdateMetaMap()
{
    size_t listCtrlIndex = listctrlMetaLog_.GetItemCount();
    size_t listCtrlDetailIndex = listctrlDetailLog_.GetItemCount();
    mapMetaLogItemsToDetailLogItems[listCtrlIndex] = listCtrlDetailIndex;
}


size_t GuiOutputHandler::AddIndicator()
{
    wxPanel *panelTemplateIndicator = new wxPanel(&panelTemplateIndicators_, wxID_ANY, wxDefaultPosition, wxSize(25, 25), wxNO_BORDER | wxTAB_TRAVERSAL);
    panelTemplateIndicator->SetBackgroundColour(wxColour("#FF676D"));
    sizerTemplateIndicators_.Add(panelTemplateIndicator, 0, wxGROW | wxALL, 5);

    wxBoxSizer* itemBoxSizer12 = new wxBoxSizer(wxVERTICAL);
    panelTemplateIndicator->SetSizer(itemBoxSizer12);

    wxStaticText* itemStaticText13 = new wxStaticText(panelTemplateIndicator, wxID_STATIC, _(""), wxDefaultPosition, wxSize(25, -1), 0);
    itemStaticText13->SetBackgroundColour(wxColour("#FF676D"));
    itemBoxSizer12->Add(itemStaticText13, 0, wxALIGN_LEFT | wxALL, 0);

    size_t id = indicatorPanelIndex_;
    indicatorPanelIndex_++;
    indicatorPanels_[id] = panelTemplateIndicator;
    indicatorLabels_[id] = itemStaticText13;

    sizerTemplateIndicators_.Layout();

    return id;
}

void GuiOutputHandler::RemoveIndicator(size_t indicator)
{
    if (indicatorPanels_.find(indicator) == indicatorPanels_.end())
        return;

    sizerTemplateIndicators_.Detach(indicatorPanels_[indicator]);

    indicatorPanels_[indicator]->Destroy();

    indicatorPanels_.erase(indicator);
    indicatorLabels_.erase(indicator);

    sizerTemplateIndicators_.Layout();
}

void GuiOutputHandler::SetIndicatorLabel(size_t indicator, wxString label)
{
    if (indicatorPanels_.find(indicator) == indicatorPanels_.end())
        return;

    indicatorLabels_[indicator]->SetLabel(label);
}

void GuiOutputHandler::SetSecure(size_t indicator)
{
    if (indicatorPanels_.find(indicator) == indicatorPanels_.end())
        return;

    indicatorPanels_[indicator]->SetBackgroundColour(wxColour(wxT("blue")));
}

#endif
