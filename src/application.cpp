/////////////////////////////////////////////////////////////////////////////
// Name:        application.cpp
// Purpose:     
// Author:      Ray Burgemeestre
// Modified by: 
// Created:     Wed 06 Nov 2013 21:57:53 CET
// RCS-ID:      
// Copyright:   
// Licence:     
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

#include <wx/socket.h>
#include <wx/evtloop.h>
#include <consoleoutputhandler.h>

////@begin includes
////@end includes

#include "application.h"
#include "socketserver.h"

////@begin XPM images
////@end XPM images


/*
 * Application instance implementation
 */

////@begin implement app

#if wxUSE_GUI == 1
IMPLEMENT_APP(Application)
#else
IMPLEMENT_APP_CONSOLE(Application)
#endif


////@end implement app



/*
 * Application type definition
 */

#if wxUSE_GUI == 1
IMPLEMENT_CLASS(Application, wxApp)
#else
IMPLEMENT_CLASS(Application, wxAppConsole)
#endif


/*
 * Application event table definition
 */

#if wxUSE_GUI == 1
BEGIN_EVENT_TABLE(Application, wxApp)
#else
BEGIN_EVENT_TABLE(Application, wxAppConsole)
#endif

////@begin Application event table entries
////@end Application event table entries

END_EVENT_TABLE()


/*
 * Constructor for Application
 */

Application::Application()
	: bindPort_{8888},
	  outputFile_{},
	  outputFileTruncateLines_{500000},
	  quiet_(false)
{
	Init();
}

/*
 * Member initialisation
 */

void Application::Init()
{
	////@begin Application member initialisation
	////@end Application member initialisation
}

/*
 * Initialisation for Application
 */

bool Application::OnInit()
{
#if wxUSE_GUI == 1
	////@begin Application initialisation
	// Remove the comment markers above and below this block
	// to make permanent changes to the code.

#if wxUSE_XPM
	wxImage::AddHandler(new wxXPMHandler);
#endif
#if wxUSE_LIBPNG
	wxImage::AddHandler(new wxPNGHandler);
#endif
#if wxUSE_LIBJPEG
	wxImage::AddHandler(new wxJPEGHandler);
#endif
#if wxUSE_GIF
	wxImage::AddHandler(new wxGIFHandler);
#endif
	HttpProxyWindow* mainWindow = new HttpProxyWindow(NULL);
	mainWindow->Show(true);
	////@end Application initialisation

#endif

	if (!ProcessCommandline())
		return false;

#if wxUSE_GUI == 1

	outputHandler_ = std::unique_ptr<OutputHandler>(new GuiOutputHandler{
			*mainWindow->panelTemplateIndicators,
			*mainWindow->sizerTemplateIndicators,
			*mainWindow->listctrlDetailLog,
			*mainWindow->listctrlMetaLog,
			*mainWindow->listctrlDetailLog,
			*mainWindow->listctrlCachedRequests,
			*mainWindow->checkboxIntercept,
			*mainWindow->checkboxUseCache
	});

	logger_ = std::unique_ptr<Logger>(new Logger(*outputHandler_.get()));
	logger_->SetOutputFile(outputFile_, outputFileTruncateLines_);
	logger_->SetNoStdout(!quiet_);

	socketServer_ = std::unique_ptr<SocketServer>(new SocketServer(bindPort_, *outputHandler_.get(), *logger_.get()));

	mainWindow->SetLogger(logger_.get());
	mainWindow->SetSocketServer(socketServer_.get());

#endif

	return true;
}

/*
 * Cleanup for Application
 */

int Application::OnExit()
{
#if wxUSE_GUI == 0
	////@begin Application cleanup
	return wxAppConsole::OnExit();
	////@end Application cleanup

#else
	return wxApp::OnExit();
#endif
}

#if wxUSE_GUI == 0
int Application::OnRun()
{
	outputHandler_ = std::unique_ptr<ConsoleOutputHandler>{new ConsoleOutputHandler{}};

	logger_ = std::unique_ptr<Logger>(new Logger(*outputHandler_.get()));

	logger_->SetOutputFile(outputFile_, outputFileTruncateLines_);
	logger_->SetNoStdout(!quiet_);

	socketServer_ = std::unique_ptr<SocketServer>{new SocketServer{bindPort_, *outputHandler_.get(), *logger_.get()}};

	ProcessPendingEvents();
	return MainLoop();
}
#endif


#include <wx/cmdline.h>

static const wxCmdLineEntryDesc g_cmdLineDesc [] =
{
    { wxCMD_LINE_SWITCH, _("h"), _("help"), _("displays help on the command line parameters."), wxCMD_LINE_VAL_NONE, wxCMD_LINE_OPTION_HELP },
    { wxCMD_LINE_OPTION, _("f"), _("file"), _("output everything to <file>."), wxCMD_LINE_VAL_STRING, wxCMD_LINE_PARAM_OPTIONAL },
    { wxCMD_LINE_OPTION, _("n"), _("number"), _("truncate file each <num> lines (default 500000)."), wxCMD_LINE_VAL_NUMBER, wxCMD_LINE_PARAM_OPTIONAL  },
    { wxCMD_LINE_OPTION, _("p"), _("port"), _("bind to port <port> (default 8888)."), wxCMD_LINE_VAL_NUMBER, wxCMD_LINE_PARAM_OPTIONAL },
	{ wxCMD_LINE_SWITCH, _("q"), _("quiet"), _("silent output (nothing to stdout)."), wxCMD_LINE_VAL_NONE, wxCMD_LINE_SWITCH },
    { wxCMD_LINE_NONE }
};

bool Application::ProcessCommandline()
{
	wxCmdLineParser parser(g_cmdLineDesc, argc, argv);
	int ret = parser.Parse();
	if (ret != 0) {
		return false;
	}

	wxString optFileOutput;
	if (parser.Found(wxT("file"), &optFileOutput)) {
		outputFile_ = optFileOutput.c_str();
	}

	long optNumberOutput;
	if (parser.Found(wxT("number"), &optNumberOutput)) {
		outputFileTruncateLines_ = optNumberOutput;
	}

	long optPortOutput;
	if (parser.Found(wxT("port"), &optPortOutput)) {
		bindPort_ = optPortOutput;
	}

	if (parser.Found(wxT("quiet"))) {
		quiet_ = true;
	}

	return true;
}

bool Application::UsesEventLoop()
{
	return true;
}

#undef wxAppConsole
